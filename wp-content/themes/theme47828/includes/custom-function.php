<?php
	// Loading child theme textdomain
	load_child_theme_textdomain( CURRENT_THEME, CHILD_DIR . '/languages' );

	// WP Pointers
	add_action('admin_enqueue_scripts', 'myHelpPointers');

	function myHelpPointers() {

	//First we define our pointers 
	$pointers = array(
		array(
			'id'       => 'xyz1', // unique id for this pointer
			'screen'   => 'options-permalink', // this is the page hook we want our pointer to show on
			'target'   => '#submit', // the css selector for the pointer to be tied to, best to use ID's
			'title'    => theme_locals("submit_permalink"),
			'content'  => theme_locals("submit_permalink_desc"),
			'position' => array( 
								'edge'   => 'top', //top, bottom, left, right
								'align'  => 'left', //top, bottom, left, right, middle
								'offset' => '0 5'
								)
			),

		array(
			'id'       => 'xyz2', // unique id for this pointer
			'screen'   => 'themes', // this is the page hook we want our pointer to show on
			'target'   => '#toplevel_page_options-framework', // the css selector for the pointer to be tied to, best to use ID's
			'title'    => theme_locals("import_sample_data"),
			'content'  => theme_locals("import_sample_data_desc"),
			'position' => array( 
								'edge'   => 'bottom', //top, bottom, left, right
								'align'  => 'top', //top, bottom, left, right, middle
								'offset' => '0 -10'
								)
			),

		array(
			'id'       => 'xyz3', // unique id for this pointer
			'screen'   => 'toplevel_page_options-framework', // this is the page hook we want our pointer to show on
			'target'   => '#toplevel_page_options-framework', // the css selector for the pointer to be tied to, best to use ID's
			'title'    => theme_locals("import_sample_data"),
			'content'  => theme_locals("import_sample_data_desc_2"),
			'position' => array( 
								'edge'   => 'left', //top, bottom, left, right
								'align'  => 'top', //top, bottom, left, right, middle
								'offset' => '0 18'
								)
			)
		// more as needed
		);

		//Now we instantiate the class and pass our pointer array to the constructor 
		$myPointers = new WP_Help_Pointer($pointers);
	};











// Box
if (!function_exists('box_shortcode')) {
	function box_shortcode($atts, $content = null) {

		$output = '<div class="box">';
			$output .= do_shortcode($content);
		$output .= '</div>';

		return $output;
	}
	add_shortcode('box', 'box_shortcode');
}














// Box 1
if (!function_exists('box_1_shortcode')) {
	function box_1_shortcode($atts, $content = null) {

		$output = '<div class="box_1">';
			$output .= do_shortcode($content);
		$output .= '</div>';

		return $output;
	}
	add_shortcode('box_1', 'box_1_shortcode');
}























add_filter( 'cherry_slider_params', 'child_slider_params' );
function child_slider_params( $params ) {
    $params['minHeight'] = '"100px"';
    $params['height'] = '"36.32%"';
return $params;
}











?>