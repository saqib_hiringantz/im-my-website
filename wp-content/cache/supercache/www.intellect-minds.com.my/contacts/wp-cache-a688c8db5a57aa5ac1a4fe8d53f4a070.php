<?php die(); ?><!DOCTYPE html>

<!--[if lt IE 7 ]><html class="ie ie6" lang="en-US" prefix="og: http://ogp.me/ns#"> <![endif]-->

<!--[if IE 7 ]><html class="ie ie7" lang="en-US" prefix="og: http://ogp.me/ns#"> <![endif]-->

<!--[if IE 8 ]><html class="ie ie8" lang="en-US" prefix="og: http://ogp.me/ns#"> <![endif]-->

<!--[if IE 9 ]><html class="ie ie9" lang="en-US" prefix="og: http://ogp.me/ns#"> <![endif]-->

<!--[if (gt IE 9)|!(IE)]><!--><html lang="en-US" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->

<head>
    <link rel="alternate" href="https://www.intellect-minds.com.my/" hreflang="en-us" />
	

	<!--<meta name="description" content="Contact Us | Recruitment and Outsourcing Services in Kuala Lumpur | " />-->

	<meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="profile" href="//gmpg.org/xfn/11" />

	
	<link rel="icon" href="http://www.intellect-minds.com.my/wp-content/themes/theme47828/favicon.ico" type="image/x-icon" />

	
	<link rel="pingback" href="https://www.intellect-minds.com.my/xmlrpc.php" />

	<link rel="alternate" type="application/rss+xml" title="Intellect Minds Best Job Recruitment Agency &amp; Consultancy in Kuala Lumpur" href="https://www.intellect-minds.com.my/feed/" />

	<link rel="alternate" type="application/atom+xml" title="Intellect Minds Best Job Recruitment Agency &amp; Consultancy in Kuala Lumpur" href="https://www.intellect-minds.com.my/feed/atom/" />

	<link rel="stylesheet" type="text/css" media="all" href="https://www.intellect-minds.com.my/wp-content/themes/theme47828/bootstrap/css/bootstrap.css" />

	<link rel="stylesheet" type="text/css" media="all" href="https://www.intellect-minds.com.my/wp-content/themes/theme47828/bootstrap/css/responsive.css" />

	<link rel="stylesheet" type="text/css" media="all" href="https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/css/camera.css" />

	<link rel="stylesheet" type="text/css" media="all" href="https://www.intellect-minds.com.my/wp-content/themes/theme47828/style.css" />
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143263984-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-143263984-1');
</script>
	

	
<!-- All in One SEO Pack 3.3.2 by Michael Torbert of Semper Fi Web Designob_start_detected [-1,-1] -->
<script type="application/ld+json" class="aioseop-schema">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://www.intellect-minds.com.my/#organization","url":"https://www.intellect-minds.com.my/","name":"Intellect Minds Best Job Recruitment Agency &amp; Consultancy in Kuala Lumpur","sameAs":[]},{"@type":"WebSite","@id":"https://www.intellect-minds.com.my/#website","url":"https://www.intellect-minds.com.my/","name":"Intellect Minds Best Job Recruitment Agency &amp; Consultancy in Kuala Lumpur","publisher":{"@id":"https://www.intellect-minds.com.my/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://www.intellect-minds.com.my/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://www.intellect-minds.com.my/contacts/#webpage","url":"https://www.intellect-minds.com.my/contacts/","inLanguage":"en-US","name":"Contacts","isPartOf":{"@id":"https://www.intellect-minds.com.my/#website"},"datePublished":"2011-07-14T19:47:05+00:00","dateModified":"2019-11-15T04:39:52+00:00"}]}</script>
<link rel="canonical" href="https://www.intellect-minds.com.my/contacts/" />
<!-- All in One SEO Pack -->

<!-- This site is optimized with the Yoast SEO plugin v7.2 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Contacts | Intellect Minds Best Job Recruitment Agency &amp; Consultancy in Kuala Lumpur</title>
<meta name="description" content="Contact Us. We&#039;d love to hear from you! To learn more about the job opening. free job portals in Malaysia | Staffing services Malaysia"/>
<link rel="canonical" href="https://www.intellect-minds.com.my/contacts/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Contact Us | Recruitment and Outsourcing Services in Kuala Lumpur" />
<meta property="og:description" content="Contact Us. We&#039;d love to hear from you! To learn more about the job opening. free job portals in Malaysia | Staffing services Malaysia" />
<meta property="og:url" content="https://www.intellect-minds.com.my/contacts/" />
<meta property="og:site_name" content="Intellect Minds Best Job Recruitment Agency &amp; Consultancy in Kuala Lumpur" />
<meta property="article:publisher" content="https://www.facebook.com/IntellectMindsPte/" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="Contact Us. We&#039;d love to hear from you! To learn more about the job opening. free job portals in Malaysia | Staffing services Malaysia" />
<meta name="twitter:title" content="Contact Us | Recruitment and Outsourcing Services in Kuala Lumpur" />
<meta name="twitter:site" content="@Intellect_Minds" />
<meta name="twitter:creator" content="@Intellect_Minds" />
<script type='application/ld+json'>{"@context":"https:\/\/schema.org","@type":"Organization","url":"https:\/\/www.intellect-minds.com.my\/","sameAs":["https:\/\/www.facebook.com\/IntellectMindsPte\/","https:\/\/www.instagram.com\/intellect_minds\/","https:\/\/www.linkedin.com\/company\/intellect-minds-pte.-ltd.\/","https:\/\/www.pinterest.com\/intellectminds\/","https:\/\/twitter.com\/Intellect_Minds"],"@id":"#organization","name":"Intellect Minds","logo":"http:\/\/www.intellect-minds.com.my\/wp-content\/uploads\/2019\/07\/intellect-minds-red-logo.png"}</script>
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//maps.googleapis.com' />
<link rel='dns-prefetch' href='//netdna.bootstrapcdn.com' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Intellect Minds Best Job Recruitment Agency &amp; Consultancy in Kuala Lumpur &raquo; Feed" href="https://www.intellect-minds.com.my/feed/" />
<link rel="alternate" type="application/rss+xml" title="Intellect Minds Best Job Recruitment Agency &amp; Consultancy in Kuala Lumpur &raquo; Comments Feed" href="https://www.intellect-minds.com.my/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Intellect Minds Best Job Recruitment Agency &amp; Consultancy in Kuala Lumpur &raquo; Contacts Comments Feed" href="https://www.intellect-minds.com.my/contacts/feed/" />
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.10.0 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<!-- Note: MonsterInsights does not track you as a logged-in site administrator to prevent site owners from accidentally skewing their own Google Analytics data.
If you are testing Google Analytics code, please do so either logged out or in the private browsing/incognito mode of your web browser. -->
<script type="text/javascript" data-cfasync="false">
	var mi_version         = '7.10.0';
	var mi_track_user      = false;
	var mi_no_track_reason = 'Note: MonsterInsights does not track you as a logged-in site administrator to prevent site owners from accidentally skewing their own Google Analytics data.\nIf you are testing Google Analytics code, please do so either logged out or in the private browsing/incognito mode of your web browser.';
	
	var disableStr = 'ga-disable-UA-143263984-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-143263984-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('send','pageview');
	} else {
		console.log( "Note: MonsterInsights does not track you as a logged-in site administrator to prevent site owners from accidentally skewing their own Google Analytics data.\nIf you are testing Google Analytics code, please do so either logged out or in the private browsing/incognito mode of your web browser." );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.intellect-minds.com.my\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.12"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='dashicons-css'  href='https://www.intellect-minds.com.my/wp-includes/css/dashicons.min.css?ver=4.9.12' type='text/css' media='all' />
<link rel='stylesheet' id='admin-bar-css'  href='https://www.intellect-minds.com.my/wp-includes/css/admin-bar.min.css?ver=4.9.12' type='text/css' media='all' />
<link rel='stylesheet' id='flexslider-css'  href='https://www.intellect-minds.com.my/wp-content/plugins/cherry-plugin/lib/js/FlexSlider/flexslider.css?ver=2.2.0' type='text/css' media='all' />
<link rel='stylesheet' id='owl-carousel-css'  href='https://www.intellect-minds.com.my/wp-content/plugins/cherry-plugin/lib/js/owl-carousel/owl.carousel.css?ver=1.24' type='text/css' media='all' />
<link rel='stylesheet' id='owl-theme-css'  href='https://www.intellect-minds.com.my/wp-content/plugins/cherry-plugin/lib/js/owl-carousel/owl.theme.css?ver=1.24' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css?ver=3.2.1' type='text/css' media='all' />
<link rel='stylesheet' id='cherry-plugin-css'  href='https://www.intellect-minds.com.my/wp-content/plugins/cherry-plugin/includes/css/cherry-plugin.css?ver=1.2.7' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='https://www.intellect-minds.com.my/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.5' type='text/css' media='all' />
<link rel='stylesheet' id='monsterinsights-vue-frontend-style-css'  href='https://www.intellect-minds.com.my/wp-content/plugins/google-analytics-for-wordpress/lite/assets/vue/css/frontend.css?ver=7.10.0' type='text/css' media='all' />
<link rel='stylesheet' id='yoast-seo-adminbar-css'  href='https://www.intellect-minds.com.my/wp-content/plugins/wordpress-seo/css/dist/adminbar-720.min.css?ver=7.2' type='text/css' media='all' />
<link rel='stylesheet' id='theme47828-css'  href='https://www.intellect-minds.com.my/wp-content/themes/theme47828/main-style.css' type='text/css' media='all' />
<link rel='stylesheet' id='magnific-popup-css'  href='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/css/magnific-popup.css?ver=0.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='options_typography_Ubuntu-css'  href='//fonts.googleapis.com/css?family=Ubuntu&#038;subset=latin' type='text/css' media='all' />
<link rel='stylesheet' id='options_typography_Archivo+Narrow-css'  href='//fonts.googleapis.com/css?family=Archivo+Narrow&#038;subset=latin' type='text/css' media='all' />
<link rel='stylesheet' id='mpce-bootstrap-grid-css'  href='https://www.intellect-minds.com.my/wp-content/plugins/motopress-content-editor/bootstrap/bootstrap-grid.min.css?ver=1.4.8' type='text/css' media='all' />
<link rel='stylesheet' id='mpce-theme-css'  href='https://www.intellect-minds.com.my/wp-content/plugins/motopress-content-editor/includes/css/theme.css?ver=1.4.8' type='text/css' media='all' />
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/jquery-1.7.2.min.js?ver=1.7.2'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/plugins/cherry-plugin/lib/js/jquery.easing.1.3.js?ver=1.3'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/plugins/cherry-plugin/lib/js/elasti-carousel/jquery.elastislide.js?ver=1.2.7'></script>
<script type='text/javascript' src='//maps.googleapis.com/maps/api/js?v=3.exp&#038;sensor=false&#038;ver=4.9.12'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/jquery-migrate-1.2.1.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-includes/js/swfobject.js?ver=2.2-20120417'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/modernizr.js?ver=2.0.6'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/jflickrfeed.js?ver=1.0'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/custom.js?ver=1.0'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/bootstrap/js/bootstrap.min.js?ver=2.3.0'></script>
<script type='text/javascript' src='//www.google.com/jsapi?ver=1.4.8'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var motopressGoogleChartsPHPData = {"motopressCE":"0"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/plugins/motopress-content-editor/includes/js/mp-google-charts.js?ver=1.4.8'></script>
<link rel='https://api.w.org/' href='https://www.intellect-minds.com.my/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.intellect-minds.com.my/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.intellect-minds.com.my/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.12" />
<link rel='shortlink' href='https://www.intellect-minds.com.my/?p=14' />
<link rel="alternate" type="application/json+oembed" href="https://www.intellect-minds.com.my/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://www.intellect-minds.com.my/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F&#038;format=xml" />
<script>
 var system_folder = 'https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/admin/data_management/',
	 CHILD_URL ='https://www.intellect-minds.com.my/wp-content/themes/theme47828',
	 PARENT_URL = 'https://www.intellect-minds.com.my/wp-content/themes/CherryFramework', 
	 CURRENT_THEME = 'theme47828'</script>
<style type='text/css'>

</style>
<style type='text/css'>
h1 { font: bold 24px/34px Ubuntu;  color:#343434; }
h2 { font: bold 24px/34px Ubuntu;  color:#343434; }
h3 { font: bold 24px/34px Ubuntu;  color:#343434; }
h4 { font: bold 14px/18px Ubuntu;  color:#ffffff; }
h5 { font: normal 18px/18px Ubuntu;  color:#b80b07; }
h6 { font: normal 18px/20px Ubuntu;  color:#333333; }
body { font-weight: normal;}
.logo_h__txt, .logo_link { font: bold 48px/48px Ubuntu;  color:#1d1d1d; }
.sf-menu > li > a { font: bold 14px/18px Archivo Narrow;  color:#ffffff; }
.nav.footer-nav a { font: normal 10px/16px Ubuntu;  color:#b2b2b2; }
</style>
<style type="text/css" media="print">#wpadminbar { display:none; }</style>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
<link rel="icon" href="https://www.intellect-minds.com.my/wp-content/uploads/2018/07/LOGO.png" sizes="32x32" />
<link rel="icon" href="https://www.intellect-minds.com.my/wp-content/uploads/2018/07/LOGO.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://www.intellect-minds.com.my/wp-content/uploads/2018/07/LOGO.png" />
<meta name="msapplication-TileImage" content="https://www.intellect-minds.com.my/wp-content/uploads/2018/07/LOGO.png" />
		<style type="text/css" id="wp-custom-css">
			body {
    font-size: 14px;
}
.header .container {
    margin-top: -38px;
}
.main-holder .box .banner-wrap{
    height: 300px;
  }
.main-holder ul.job_listings li.job_listing a img.company_logo{
width: 25px;
 height: 25px;
}
.post__holder .featured-thumbnail.large { display: none; }
.header .header_widget h4
{
	margin-top: 45px!important;
}
#content {
    padding-top: 15px;
}
/*Updated by Deepak*/
.google-map{
	    border: 1px solid #d6d0d0;
    box-shadow: 2px 2px 1px 1px darkgrey;
}
@media (max-width: 767px){
.header .logo {
    float: none;
    margin: 25px 0 -20px 0;
    text-align: center;
	}
	.main-holder .box .banner-wrap {
    height: 350px !important;
}
.span2 > h6{
	text-align:center;
}
ul.clients {
	margin: 01px !important;
}
ul.clients li {
    margin: 10px 20px 10px 7px !important;
}
	.google-map{
	    border: 1px solid #d6d0d0;
    box-shadow: 2px 2px 1px 1px darkgrey;
		margin-bottom:10px;
}
	.size-full {
		width: 100%;
	}
	.main-holder .search-form input[type="submit"] {
    width: 46px!important;
	}
	.wpcf7-validates-as-tel{
		width: 192px!important;
    height: 30px!important;
    border-radius: 0px !important;
    background: #f4f4f4!important;
	}

}
/*23-10-2019*/
	.wpcf7-validates-as-tel{
		width: 192px!important;
    height: 30px!important;
    border-radius: 0px !important;
    background: #f4f4f4!important;
	}
@media(min-width:768px){
	.divright{
     position: absolute!important;
    left: 615px!important;
}
}
		</style>
	
	
	<!--[if lt IE 9]>

		<div id="ie7-alert" style="width: 100%; text-align:center;">

			<img src="http://tmbhtest.com/images/ie7.jpg" alt="Upgrade IE 8" width="640" height="344" border="0" usemap="#Map" />

			<map name="Map" id="Map"><area shape="rect" coords="496,201,604,329" href="http://www.microsoft.com/windows/internet-explorer/default.aspx" target="_blank" alt="Download Interent Explorer" /><area shape="rect" coords="380,201,488,329" href="http://www.apple.com/safari/download/" target="_blank" alt="Download Apple Safari" /><area shape="rect" coords="268,202,376,330" href="http://www.opera.com/download/" target="_blank" alt="Download Opera" /><area shape="rect" coords="155,202,263,330" href="http://www.mozilla.com/" target="_blank" alt="Download Firefox" /><area shape="rect" coords="35,201,143,329" href="http://www.google.com/chrome" target="_blank" alt="Download Google Chrome" />

			</map>

		</div>

	<![endif]-->

	<!--[if gte IE 9]><!-->
	

		<script src="https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/jquery.mobile.customized.min.js" type="text/javascript"></script>

		<script type="text/javascript">

			jQuery(function(){

				jQuery('.sf-menu').mobileMenu({defaultText: "Navigate to..."});

			});

		</script>

	<!--<![endif]-->

	<script type="text/javascript">

		// Init navigation menu

		jQuery(function(){

		// main navigation init

			jQuery('ul.sf-menu').superfish({

				delay: 1000, // the delay in milliseconds that the mouse can remain outside a sub-menu without it closing

				animation: {

					opacity: "show",

					height: "show"

				}, // used to animate the sub-menu open

				speed: "normal", // animation speed

				autoArrows: true, // generation of arrow mark-up (for submenu)

				disableHI: true // to disable hoverIntent detection

			});



		//Zoom fix

		//IPad/IPhone

			var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),

				ua = navigator.userAgent,

				gestureStart = function () {

					viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0";

				},

				scaleFix = function () {

					if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {

						viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";

						document.addEventListener("gesturestart", gestureStart, false);

					}

				};

			scaleFix();

		})

	</script>

	<!-- stick up menu -->

	<script type="text/javascript">

		jQuery(document).ready(function(){

			if(!device.mobile() && !device.tablet()){

				jQuery('.header .nav__primary').tmStickUp({

					correctionSelector: jQuery('#wpadminbar')

				,	listenSelector: jQuery('.listenSelector')

				,	active: false
				,	pseudo: true
				});

			}

		})

	</script>



<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');



  ga('create', 'UA-74588312-1', 'auto');

  ga('send', 'pageview');



</script>

</head>



<body class="page-template page-template-page-fullwidth page-template-page-fullwidth-php page page-id-14 logged-in admin-bar no-customize-support theme47828">

	<div id="motopress-main" class="main-holder">

		<!--Begin #motopress-main-->

		<header class="motopress-wrapper header">

			<div class="container">

				<div class="row">

					<div class="span12" data-motopress-wrapper-file="wrapper/wrapper-header.php" data-motopress-wrapper-type="header" data-motopress-id="5dce30f85dd15">

						<div class="row">
	<div class="span6" data-motopress-type="static" data-motopress-static-file="static/static-logo.php">
		<!-- BEGIN LOGO -->
<div class="logo pull-left">
									<a href="https://www.intellect-minds.com.my/" class="logo_h logo_h__img"><img src="http://www.intellect-minds.com.my/wp-content/uploads/2019/07/intellect-minds-red-logo.png" alt="Intellect Minds Best Job Recruitment Agency &amp; Consultancy in Kuala Lumpur" title=""></a>
				</div>
<!-- END LOGO -->	</div>
	<div class="span6">
		<div class="header_widget" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="header-sidebar">
					</div>
		<div class="hidden-phone" data-motopress-type="static" data-motopress-static-file="static/static-search.php">
			<!-- BEGIN SEARCH FORM -->
	<div class="search-form search-form__h hidden-phone clearfix">
		<form id="search-header" class="navbar-form pull-right" method="get" action="https://www.intellect-minds.com.my/" accept-charset="utf-8">
			<input type="text" name="s" placeholder="search" class="search-form_it">
			<input type="submit" value="Go" id="search-form_is" class="search-form_is btn btn-primary">
		</form>
	</div>
<!-- END SEARCH FORM -->		</div>
	</div>
</div>
<div class="spacer"></div>
<div class="row">
	<div class="span8" data-motopress-type="static" data-motopress-static-file="static/static-nav.php">
		<!-- BEGIN MAIN NAVIGATION -->
<nav class="nav nav__primary clearfix">
<ul id="topnav" class="sf-menu"><li id="menu-item-1807" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home"><a href="https://www.intellect-minds.com.my/">Home</a></li>
<li id="menu-item-3631" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="https://www.intellect-minds.com.my/expertise/">Expertise</a>
<ul class="sub-menu">
	<li id="menu-item-1811" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.intellect-minds.com.my/expertise/information-technology/">Information Technology</a></li>
	<li id="menu-item-1812" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.intellect-minds.com.my/expertise/banking-finance/">Banking &#038; Finance</a></li>
	<li id="menu-item-1805" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.intellect-minds.com.my/expertise/managed-services/">Managed Services</a></li>
</ul>
</li>
<li id="menu-item-2863" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="#">Clients</a>
<ul class="sub-menu">
	<li id="menu-item-2075" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.intellect-minds.com.my/service-offerings-engineering/">Service Offerings</a></li>
	<li id="menu-item-2097" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.intellect-minds.com.my/why-engage-us/">Why engage us?</a></li>
	<li id="menu-item-2096" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.intellect-minds.com.my/suggestions/">Suggestions</a></li>
</ul>
</li>
<li id="menu-item-2905" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="#">Jobseekers</a>
<ul class="sub-menu">
	<li id="menu-item-2120" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.intellect-minds.com.my/current-openings/">Current Openings</a></li>
	<li id="menu-item-2118" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.intellect-minds.com.my/quick-submission/">Quick Submission</a></li>
	<li id="menu-item-2117" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.intellect-minds.com.my/join-us/">Join Us</a></li>
</ul>
</li>
<li id="menu-item-1804" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-14 current_page_item"><a href="https://www.intellect-minds.com.my/contacts/">Contacts</a></li>
<li id="menu-item-3039" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.intellect-minds.com.my/blog/">Blog</a></li>
</ul></nav><!-- END MAIN NAVIGATION -->	</div>
	<!-- Social Links -->
	<div class="span4 social-nets-wrapper" data-motopress-type="static" data-motopress-static-file="static/static-social-networks.php">
		<ul class="social">
	<li><a href="https://twitter.com/Intellect_Minds" title="twitter"><img src="http://www.intellect-minds.com.my/wp-content/themes/theme47828/images/social/twitter.png" alt="twitter"></a></li><li><a href="https://www.facebook.com/IntellectMindsPteLtd/" title="facebook"><img src="http://www.intellect-minds.com.my/wp-content/themes/theme47828/images/social/facebook.png" alt="facebook"></a></li><li><a href="mailto:info@intellect-minds.com" title="mail"><img src="http://www.intellect-minds.com.my/wp-content/themes/theme47828/images/social/mail.png" alt="mail"></a></li><li><a href="#" title="google"><img src="http://www.intellect-minds.com.my/wp-content/themes/theme47828/images/social/google.png" alt="google"></a></li></ul>	</div>
	<!-- /Social Links -->
</div>
					</div>

				</div>

			</div>

		</header>
<div class="motopress-wrapper content-holder clearfix">
	<div class="container">
		<div class="row">
			<div class="span12" data-motopress-wrapper-file="page-fullwidth.php" data-motopress-wrapper-type="content">
				<div class="row">
					<div class="span12" data-motopress-type="static" data-motopress-static-file="static/static-title.php">
						<section class="title-section">
	<h1 class="title-header">
					Contacts	</h1>
				<!-- BEGIN BREADCRUMBS-->
			<ul class="breadcrumb breadcrumb__t"><li><a href="https://www.intellect-minds.com.my">Home</a></li><li class="divider"></li><li class="active">Contacts</li></ul>			<!-- END BREADCRUMBS -->
	</section><!-- .title-section -->
					</div>
				</div>
				<div id="content" class="row">
					<div class="span12" data-motopress-type="loop" data-motopress-loop-file="loop/loop-page.php">
							<div id="post-14" class="page post-14 type-page status-publish hentry">
		<div class="row ">
<div class="span12 "><div class="span6 "><div class="google-map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3983.755163738746!2d101.71754031475734!3d3.15911529769975!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x702a65abfd2fe07d!2sBest%20Job%20Recruitment%20Agency%20and%20Consultancy%20in%20Malaysia!5e0!3m2!1sen!2sin!4v1573726132331!5m2!1sen!2sin" frameborder="0" width="100%" height="300" marginwidth="0" marginheight="0" scrolling="no"></iframe></div></div>
<div class="divright" style="float: right;"><div class="span6 "><div class="google-map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.829385720733!2d103.84360971475394!3d1.2757154990695354!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da191340c98963%3A0x7c483a60d4d423bb!2sIntellect%20Minds%20Pte%20Ltd%20-%20Best%20Job%20Recruitment%20Agency%20and%20Consultancy%20in%20Singapore!5e0!3m2!1sen!2ssg!4v1573725746031!5m2!1sen!2ssg" frameborder="0" width="100%" height="300" marginwidth="0" marginheight="0" scrolling="no"></iframe></div></div></div></div>
<div>
<div class="span12 "><div class="Container">
<div class="col-md-6" style="width: 540px;">
<div class="span6 "><h2>Malaysia</h2>
<h4><span style="color: #333333;">Office/Postal Address</span></h4>
<p>Level 14-02, GTower, 199, Jalan Tun Razak, 50400 Kuala Lumpur.</p>
<p>Telephone:      +60 03 2168  1954<br />
EXT:            8447 &amp; 8451<br />
E-mail:         <a href="mailto:info@intellect-minds.com">info@intellect-minds.com</a><br /></div>
</div>
<div class="col-md-6" style="float: right; width: 560px;">
<div class="span6 "><h2>Singapore</h2>
<h4><span style="color: #333333;">Office Address</span></h4>
<p>#05-51, Anson Center, 51 Anson Road Singapore - 079904</p>
<p>Telephone:    +65 - 6222 0421 / 9016 / 6703<br />
FAX:    +65 - 6222 9011<br />
E-mail:     <a href="mailto:info@intellect-minds.com">info@intellect-minds.com</a><br /></div>
</div>
</div></div>
</div>
</div><!-- .row (end) -->
<h2>Contact form</h2>
<div role="form" class="wpcf7" id="wpcf7-f208-p14-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/contacts/#wpcf7-f208-p14-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="208" />
<input type="hidden" name="_wpcf7_version" value="5.1.5" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f208-p14-o1" />
<input type="hidden" name="_wpcf7_container_post" value="14" />
</div>
<div class="row-fluid">
<p class="span4 field"><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Name:" /></span> </p>
<p class="span4 field"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail:" /></span> </p>
<p class="span4 field"><span class="wpcf7-form-control-wrap your-phone"><input type="text" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Phone:" /></span> </p>
</div>
<p class="field"><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message:"></textarea></span> </p>
<p class="submit-wrap"><input type="reset" value="clear" class="btn btn-primary" /><input type="submit" value="send" class="wpcf7-form-control wpcf7-submit btn btn-primary" /></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
		<div class="clear"></div>
		<!--.pagination-->
	</div><!--post-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

		<footer class="motopress-wrapper footer">
			<div class="container">
				<div class="row">
					<div class="span12" data-motopress-wrapper-file="wrapper/wrapper-footer.php" data-motopress-wrapper-type="footer" data-motopress-id="5dce30f87b7db">
						<div class="row footer-widgets">
	<div class="span2" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-1">
			</div>
	<div class="span2" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-2">
			</div>
	<div class="span2" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-3">
			</div>
	<div class="span2" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-4">
			</div>
	<div class="span4">
		<div class="indent_left">
			<div data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-5">
							</div>
			<div data-motopress-type="static" data-motopress-static-file="static/static-footer-text.php">
				<div id="footer-text" class="footer-text">
		
			©  Copyright 2019. All Rights Reserved.		</div>			</div>
			<div data-motopress-type="static" data-motopress-static-file="static/static-footer-nav.php">
							</div>
		</div>
	</div>
</div>					</div>
				</div>
			</div>
		</footer>
		<!--End #motopress-main-->
	</div>
	<div id="back-top-wrapper" class="visible-desktop">
		<p id="back-top">
			<a href="#top"><span></span></a>		</p>
	</div>
		<link rel='stylesheet' id='bxSlidercss-css'  href='https://www.intellect-minds.com.my/wp-content/plugins/slider-image_old/style/jquery.bxslider.css?ver=4.9.12' type='text/css' media='all' />
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-includes/js/admin-bar.min.js?ver=4.9.12'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-includes/js/comment-reply.min.js?ver=4.9.12'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/plugins/cherry-plugin/lib/js/FlexSlider/jquery.flexslider-min.js?ver=2.2.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var items_custom = [[0,1],[480,2],[768,3],[980,4],[1170,5]];
/* ]]> */
</script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/plugins/cherry-plugin/includes/js/cherry-plugin.js?ver=1.2.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.intellect-minds.com.my\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.5'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/plugins/google-analytics-for-wordpress/lite/assets/vue/js/chunk-frontend-vendors.js?ver=7.10.0'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/plugins/google-analytics-for-wordpress/lite/assets/vue/js/chunk-common.js?ver=7.10.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var monsterinsights = {"ajax":"https:\/\/www.intellect-minds.com.my\/wp-admin\/admin-ajax.php","nonce":"84dbfb1b54","network":"","translations":{"":{"domain":"google-analytics-for-wordpress","lang":"en_US"}},"assets":"https:\/\/www.intellect-minds.com.my\/wp-content\/plugins\/google-analytics-for-wordpress\/lite\/assets\/vue","addons_url":"https:\/\/www.intellect-minds.com.my\/wp-admin\/admin.php?page=monsterinsights_settings#\/addons","page_id":"14","page_title":"Contacts","plugin_version":"7.10.0","shareasale_id":"0","shareasale_url":"https:\/\/www.shareasale.com\/r.cfm?B=971799&U=0&M=69975&urllink=","is_admin":"","reports_url":"https:\/\/www.intellect-minds.com.my\/wp-admin\/admin.php?page=monsterinsights_reports","authed":"1","getting_started_url":"https:\/\/www.intellect-minds.com.my\/wp-admin\/admin.php?page=monsterinsights_settings#\/about\/getting-started","wizard_url":"https:\/\/www.intellect-minds.com.my\/wp-admin\/index.php?page=monsterinsights-onboarding"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/plugins/google-analytics-for-wordpress/lite/assets/vue/js/frontend.js?ver=7.10.0'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/superfish.js?ver=1.5.3'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/jquery.mobilemenu.js?ver=1.0'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/jquery.magnific-popup.min.js?ver=0.9.3'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/jplayer.playlist.min.js?ver=2.3.0'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/jquery.jplayer.min.js?ver=2.6.0'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/tmstickup.js?ver=1.0.0'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/device.min.js?ver=1.0.0'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/jquery.zaccordion.min.js?ver=2.1.0'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/themes/CherryFramework/js/camera.min.js?ver=1.3.4'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-includes/js/wp-embed.min.js?ver=4.9.12'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/plugins/slider-image_old/js/jquery.bxslider.js?ver=1.0.0'></script>
<script type='text/javascript' src='https://www.intellect-minds.com.my/wp-content/plugins/slider-image_old/js/bxslider.setup.js?ver=1.0.0'></script>
			<script type="text/javascript">
				deleteCookie('cf-cookie-banner');
			</script>
				<!--[if lte IE 8]>
		<script type="text/javascript">
			document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
		</script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
		<script type="text/javascript">
			(function() {
				var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

						request = true;
		
				b[c] = b[c].replace( rcs, ' ' );
				// The customizer requires postMessage and CORS (if the site is cross domain)
				b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
			}());
		</script>
	<!--<![endif]-->
			<div id="wpadminbar" class="nojq nojs">
							<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Skip to toolbar</a>
						<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Toolbar" tabindex="0">
				<ul id="wp-admin-bar-root-default" class="ab-top-menu">
		<li id="wp-admin-bar-wp-logo" class="menupop"><a class="ab-item" aria-haspopup="true" href="https://www.intellect-minds.com.my/wp-admin/about.php"><span class="ab-icon"></span><span class="screen-reader-text">About WordPress</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
		<li id="wp-admin-bar-about"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/about.php">About WordPress</a>		</li></ul><ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
		<li id="wp-admin-bar-wporg"><a class="ab-item" href="https://wordpress.org/">WordPress.org</a>		</li>
		<li id="wp-admin-bar-documentation"><a class="ab-item" href="https://codex.wordpress.org/">Documentation</a>		</li>
		<li id="wp-admin-bar-support-forums"><a class="ab-item" href="https://wordpress.org/support/">Support Forums</a>		</li>
		<li id="wp-admin-bar-feedback"><a class="ab-item" href="https://wordpress.org/support/forum/requests-and-feedback">Feedback</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-site-name" class="menupop"><a class="ab-item" aria-haspopup="true" href="https://www.intellect-minds.com.my/wp-admin/">Intellect Minds Best Job Recruitment Age&hellip;</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-site-name-default" class="ab-submenu">
		<li id="wp-admin-bar-dashboard"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/">Dashboard</a>		</li></ul><ul id="wp-admin-bar-appearance" class="ab-submenu">
		<li id="wp-admin-bar-themes"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/themes.php">Themes</a>		</li>
		<li id="wp-admin-bar-widgets"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/widgets.php">Widgets</a>		</li>
		<li id="wp-admin-bar-menus"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/nav-menus.php">Menus</a>		</li>
		<li id="wp-admin-bar-of_theme_options"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/admin.php?page=options-framework">Cherry Options</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-customize" class="hide-if-no-customize"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/customize.php?url=https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F">Customize</a>		</li>
		<li id="wp-admin-bar-updates"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/update-core.php" title="4 Theme Updates"><span class="ab-icon"></span><span class="ab-label">4</span><span class="screen-reader-text">4 Theme Updates</span></a>		</li>
		<li id="wp-admin-bar-comments"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/edit-comments.php"><span class="ab-icon"></span><span class="ab-label awaiting-mod pending-count count-0" aria-hidden="true">0</span><span class="screen-reader-text">0 comments awaiting moderation</span></a>		</li>
		<li id="wp-admin-bar-new-content" class="menupop"><a class="ab-item" aria-haspopup="true" href="https://www.intellect-minds.com.my/wp-admin/post-new.php"><span class="ab-icon"></span><span class="ab-label">New</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-new-content-default" class="ab-submenu">
		<li id="wp-admin-bar-new-post"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/post-new.php">Post</a>		</li>
		<li id="wp-admin-bar-new-media"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/media-new.php">Media</a>		</li>
		<li id="wp-admin-bar-new-page"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/post-new.php?post_type=page">Page</a>		</li>
		<li id="wp-admin-bar-new-job_listing"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/post-new.php?post_type=job_listing">Job</a>		</li>
		<li id="wp-admin-bar-new-slider"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/post-new.php?post_type=slider">Slides</a>		</li>
		<li id="wp-admin-bar-new-portfolio"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/post-new.php?post_type=portfolio">Portfolio</a>		</li>
		<li id="wp-admin-bar-new-testi"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/post-new.php?post_type=testi">Testimonial</a>		</li>
		<li id="wp-admin-bar-new-services"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/post-new.php?post_type=services">Services</a>		</li>
		<li id="wp-admin-bar-new-clients"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/post-new.php?post_type=clients">Clients</a>		</li>
		<li id="wp-admin-bar-new-faq"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/post-new.php?post_type=faq">FAQs</a>		</li>
		<li id="wp-admin-bar-new-team"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/post-new.php?post_type=team">Our Team</a>		</li>
		<li id="wp-admin-bar-new-user"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/user-new.php">User</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-edit"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/post.php?post=14&#038;action=edit">Edit Page</a>		</li>
		<li id="wp-admin-bar-motopress-edit"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/post.php?post=14&#038;action=edit&#038;motopress-ce-auto-open=true" onclick="sessionStorage.setItem(&quot;motopressPluginAutoOpen&quot;, true);" title="Edit with MotoPress">Edit with MotoPress</a>		</li>
		<li id="wp-admin-bar-wpseo-menu" class="menupop"><a class="ab-item" aria-haspopup="true" href="https://www.intellect-minds.com.my/wp-admin/admin.php?page=wpseo_dashboard"><div id="yoast-ab-icon" class="ab-item yoast-logo svg"><span class="screen-reader-text">SEO</span></div><div class="wpseo-score-icon adminbar-seo-score bad"><span class="adminbar-seo-score-text screen-reader-text"></span></div></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wpseo-menu-default" class="ab-submenu">
		<li id="wp-admin-bar-wpseo-configuration-wizard"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/admin.php?page=wpseo_configurator">Configuration Wizard</a>		</li>
		<li id="wp-admin-bar-wpseo-kwresearch" class="menupop"><div class="ab-item ab-empty-item" tabindex="0" aria-haspopup="true">Keyword Research</div><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wpseo-kwresearch-default" class="ab-submenu">
		<li id="wp-admin-bar-wpseo-adwordsexternal"><a class="ab-item" href="https://adwords.google.com/keywordplanner" target="_blank">AdWords External</a>		</li>
		<li id="wp-admin-bar-wpseo-googleinsights"><a class="ab-item" href="https://www.google.com/trends/explore#q=free+job+portals+in+Malaysia%2C+Staffing+services+Malaysia%2C+Free+Job+Portals" target="_blank">Google Trends</a>		</li>
		<li id="wp-admin-bar-wpseo-wordtracker"><a class="ab-item" href="http://tools.seobook.com/keyword-tools/seobook/?keyword=free+job+portals+in+Malaysia%2C+Staffing+services+Malaysia%2C+Free+Job+Portals" target="_blank">SEO Book</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-wpseo-analysis" class="menupop"><div class="ab-item ab-empty-item" tabindex="0" aria-haspopup="true">Analyze this page</div><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wpseo-analysis-default" class="ab-submenu">
		<li id="wp-admin-bar-wpseo-inlinks-ose"><a class="ab-item" href="//moz.com/researchtools/ose/links?site=https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F" target="_blank">Check Inlinks (OSE)</a>		</li>
		<li id="wp-admin-bar-wpseo-kwdensity"><a class="ab-item" href="http://www.zippy.co.uk/keyworddensity/index.php?url=https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F&#038;keyword=free+job+portals+in+Malaysia%2C+Staffing+services+Malaysia%2C+Free+Job+Portals" target="_blank">Check Keyword Density</a>		</li>
		<li id="wp-admin-bar-wpseo-cache"><a class="ab-item" href="//webcache.googleusercontent.com/search?strip=1&#038;q=cache:https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F" target="_blank">Check Google Cache</a>		</li>
		<li id="wp-admin-bar-wpseo-header"><a class="ab-item" href="//quixapp.com/headers/?r=https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F" target="_blank">Check Headers</a>		</li>
		<li id="wp-admin-bar-wpseo-structureddata"><a class="ab-item" href="https://search.google.com/structured-data/testing-tool#url=https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F" target="_blank">Google Structured Data Test</a>		</li>
		<li id="wp-admin-bar-wpseo-facebookdebug"><a class="ab-item" href="//developers.facebook.com/tools/debug/og/object?q=https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F" target="_blank">Facebook Debugger</a>		</li>
		<li id="wp-admin-bar-wpseo-pinterestvalidator"><a class="ab-item" href="https://developers.pinterest.com/tools/url-debugger/?link=https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F" target="_blank">Pinterest Rich Pins Validator</a>		</li>
		<li id="wp-admin-bar-wpseo-htmlvalidation"><a class="ab-item" href="//validator.w3.org/check?uri=https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F" target="_blank">HTML Validator</a>		</li>
		<li id="wp-admin-bar-wpseo-cssvalidation"><a class="ab-item" href="//jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F" target="_blank">CSS Validator</a>		</li>
		<li id="wp-admin-bar-wpseo-pagespeed"><a class="ab-item" href="//developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F" target="_blank">Google Page Speed Test</a>		</li>
		<li id="wp-admin-bar-wpseo-google-mobile-friendly"><a class="ab-item" href="https://www.google.com/webmasters/tools/mobile-friendly/?url=https%3A%2F%2Fwww.intellect-minds.com.my%2Fcontacts%2F" target="_blank">Mobile-Friendly Test</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-wpseo-settings" class="menupop"><div class="ab-item ab-empty-item" tabindex="0" aria-haspopup="true">SEO Settings</div><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wpseo-settings-default" class="ab-submenu">
		<li id="wp-admin-bar-wpseo-general"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/admin.php?page=wpseo_dashboard">Dashboard</a>		</li>
		<li id="wp-admin-bar-wpseo-titles"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/admin.php?page=wpseo_titles">Search Appearance</a>		</li>
		<li id="wp-admin-bar-wpseo-search-console"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/admin.php?page=wpseo_search_console">Search Console</a>		</li>
		<li id="wp-admin-bar-wpseo-social"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/admin.php?page=wpseo_social">Social</a>		</li>
		<li id="wp-admin-bar-wpseo-tools"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/admin.php?page=wpseo_tools">Tools</a>		</li>
		<li id="wp-admin-bar-wpseo-licenses"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/admin.php?page=wpseo_licenses">Premium</a>		</li></ul></div>		</li></ul></div>		</li>
		<li id="wp-admin-bar-delete-cache"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/index.php?action=delcachepage&#038;path=%2Fcontacts%2F&#038;_wpnonce=789d730f41" title="Delete cache of the current page">Delete Cache</a>		</li>
		<li id="wp-admin-bar-monsterinsights_frontend_button"><a class="ab-item" href="#"><span class="ab-icon dashicons-before dashicons-chart-bar"></span> Insights</a>		</li>
		<li id="wp-admin-bar-all-in-one-seo-pack" class="menupop"><a class="ab-item" aria-haspopup="true" href="https://www.intellect-minds.com.my/wp-admin/admin.php?page=all-in-one-seo-pack/aioseop_class.php">SEO</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-all-in-one-seo-pack-default" class="ab-submenu">
		<li id="wp-admin-bar-aioseop-pro-upgrade"><a class="ab-item" href="https://semperplugins.com/plugins/all-in-one-seo-pack-pro-version/?loc=menu" target="_blank">Upgrade To Pro</a>		</li>
		<li id="wp-admin-bar-aiosp_edit_14"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/post.php?post=14&#038;action=edit#aiosp">Edit SEO</a>		</li>
		<li id="wp-admin-bar-all-in-one-seo-pack/modules/aioseop_performance.php"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/admin.php?page=all-in-one-seo-pack/modules/aioseop_performance.php">Performance</a>		</li>
		<li id="wp-admin-bar-all-in-one-seo-pack/modules/aioseop_feature_manager.php"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/admin.php?page=all-in-one-seo-pack/modules/aioseop_feature_manager.php">Feature Manager</a>		</li></ul></div>		</li></ul><ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
		<li id="wp-admin-bar-search" class="admin-bar-search"><div class="ab-item ab-empty-item" tabindex="-1"><form action="https://www.intellect-minds.com.my/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">Search</label><input type="submit" class="adminbar-button" value="Search"/></form></div>		</li>
		<li id="wp-admin-bar-my-account" class="menupop with-avatar"><a class="ab-item" aria-haspopup="true" href="https://www.intellect-minds.com.my/wp-admin/profile.php">Howdy, <span class="display-name">admin</span><img alt='' src='https://secure.gravatar.com/avatar/e64df3de6bd191c3eaab6cee9e02109d?s=26&#038;d=mm&#038;r=g' srcset='https://secure.gravatar.com/avatar/e64df3de6bd191c3eaab6cee9e02109d?s=52&#038;d=mm&#038;r=g 2x' class='avatar avatar-26 photo' height='26' width='26' /></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-user-actions" class="ab-submenu">
		<li id="wp-admin-bar-user-info"><a class="ab-item" tabindex="-1" href="https://www.intellect-minds.com.my/wp-admin/profile.php"><img alt='' src='https://secure.gravatar.com/avatar/e64df3de6bd191c3eaab6cee9e02109d?s=64&#038;d=mm&#038;r=g' srcset='https://secure.gravatar.com/avatar/e64df3de6bd191c3eaab6cee9e02109d?s=128&#038;d=mm&#038;r=g 2x' class='avatar avatar-64 photo' height='64' width='64' /><span class='display-name'>admin</span></a>		</li>
		<li id="wp-admin-bar-edit-profile"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-admin/profile.php">Edit My Profile</a>		</li>
		<li id="wp-admin-bar-logout"><a class="ab-item" href="https://www.intellect-minds.com.my/wp-login.php?action=logout&#038;_wpnonce=031bfe65e1">Log Out</a>		</li></ul></div>		</li></ul>			</div>
						<a class="screen-reader-shortcut" href="https://www.intellect-minds.com.my/wp-login.php?action=logout&#038;_wpnonce=031bfe65e1">Log Out</a>
					</div>

		 <!-- this is used by many Wordpress features and for plugins to work properly -->
</body>
</html>
<!-- Dynamic page generated in 0.862 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2019-11-15 05:00:40 -->
