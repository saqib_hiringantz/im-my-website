<?php die(); ?><?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="//www.intellect-minds.com.my/wp-content/plugins/wordpress-seo/css/main-sitemap.xsl"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<sitemap>
		<loc>https://www.intellect-minds.com.my/post-sitemap.xml</loc>
		<lastmod>2019-11-14T05:54:29+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://www.intellect-minds.com.my/page-sitemap.xml</loc>
		<lastmod>2019-11-15T04:39:52+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://www.intellect-minds.com.my/attachment-sitemap.xml</loc>
		<lastmod>2019-11-14T05:52:33+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://www.intellect-minds.com.my/job_listing-sitemap.xml</loc>
		<lastmod>2019-10-25T11:49:05+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://www.intellect-minds.com.my/slider-sitemap.xml</loc>
		<lastmod>2015-03-04T14:37:47+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://www.intellect-minds.com.my/portfolio-sitemap.xml</loc>
		<lastmod>2013-11-18T13:52:31+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://www.intellect-minds.com.my/testi-sitemap.xml</loc>
		<lastmod>2016-03-21T16:25:34+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://www.intellect-minds.com.my/clients-sitemap.xml</loc>
		<lastmod>2015-03-26T12:27:10+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://www.intellect-minds.com.my/team-sitemap.xml</loc>
		<lastmod>2013-11-15T16:52:49+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://www.intellect-minds.com.my/category-sitemap.xml</loc>
		<lastmod>2019-11-14T05:54:29+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://www.intellect-minds.com.my/portfolio_category-sitemap.xml</loc>
		<lastmod>2013-11-18T13:52:31+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://www.intellect-minds.com.my/portfolio_tag-sitemap.xml</loc>
		<lastmod>2013-11-18T13:52:31+00:00</lastmod>
	</sitemap>
</sitemapindex>
<!-- XML Sitemap generated by Yoast SEO -->